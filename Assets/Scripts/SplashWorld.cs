using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// SplashWorld enabled/disabled by PageGame (ie. while InSplashWorld),
/// so will start and stop listening accordingly
/// </summary>
///

public class SplashWorld : MonoBehaviour
{
    [SerializeField]
    private Camera pageCamera;

    [SerializeField]
    private LayerMask raycastLayers;            // for 'touching' objects

    private float raycastDistance = 1000f;

    [SerializeField]
    // if true, page camera rotation set exactly as per AR camera, regardless of cameraStartPose rotation
    // if false, page camera rotates with AR camera, but relative to cameraStartPose rotation
    private bool mirrorARCamera = false;

    [SerializeField]
    private float ARCameraMoveFactor = 10f;      // to amplify AR camera movement

    private Pose cameraStartPose;

    [SerializeField]
    private Light pageLight;

    [Header("Audio")]

    [SerializeField]
    private AudioSource musicSource;

    [SerializeField]
    private AudioSource sfxSource;

    [SerializeField]
    private AudioClip gameMusic;

    [SerializeField]
    private AudioClip startGameAudio;

    [SerializeField]
    private AudioClip completeGameAudio;

    [Space]

    [SerializeField]
    private Transform exitObject;          // tap to complete splash game  // TODO: temp!


    private void Start()
    {
        cameraStartPose.position = pageCamera.transform.position;
        cameraStartPose.rotation = pageCamera.transform.rotation;
    }

    private void OnEnable()
    {
        GameEvents.OnARCameraPoseChanged += OnARCameraPoseChanged;
        GameEvents.OnTapOn += OnTouchOn;

        PlaySFX(startGameAudio);
        PlayMusic();
    }

    private void OnDisable()
    {
        GameEvents.OnARCameraPoseChanged -= OnARCameraPoseChanged;
        GameEvents.OnTapOn -= OnTouchOn;

        StopMusic();
    }


    private void Update()
    {
        CameraRaycast();
    }

    private void CameraRaycast()
    {
        // raycast to detect what camera centre is 'looking at'
        RaycastHit hitInfo;
        Physics.Raycast(pageCamera.transform.position, pageCamera.transform.forward, out hitInfo, raycastDistance, raycastLayers);

        GameEvents.OnSplashWorldLookAtRaycast?.Invoke(hitInfo.transform);      // may be null!
    }

    // sync page camera to AR camera
    private void OnARCameraPoseChanged(Pose ARCameraPose, Pose ARCameraPoseDelta)
    {
        // set page camera position relative to original position
        pageCamera.transform.position = cameraStartPose.position + (ARCameraPoseDelta.position * ARCameraMoveFactor);

        if (mirrorARCamera)
        {
            pageCamera.transform.rotation = ARCameraPose.rotation;
        }
        else
        {
            // subtract rotation from start rotation.. like this...
            pageCamera.transform.rotation = cameraStartPose.rotation * Quaternion.Inverse(ARCameraPoseDelta.rotation);

            //// add AR camera rotation delta to page camera start rotation
            //pageCamera.transform.rotation = cameraStartPose.rotation * ARCameraPoseDelta.rotation;
        }
    }


    private void OnTouchOn(Vector2 screenPosition, int touchCount)
    {
        Ray ray = pageCamera.ScreenPointToRay(screenPosition);

        // raycast to detect what was touched
        RaycastHit hitInfo;
        Physics.Raycast(ray, out hitInfo, raycastDistance, raycastLayers);

        if (hitInfo.transform != null)
        {
            bool touchedExit = exitObject != null && hitInfo.transform == exitObject;

            if (touchedExit)
                PlaySFX(completeGameAudio);

            GameEvents.OnSplashWorldTapRaycast?.Invoke(hitInfo.transform, touchedExit);
        }
    }

    private void PlayMusic()
    {
        if (musicSource != null && gameMusic != null)
        {
            musicSource.loop = true;
            musicSource.clip = gameMusic;
            musicSource.Play();
        }
    }

    private void StopMusic()
    {
        if (musicSource != null && gameMusic != null)
        {
            musicSource.Stop();
        }
    }

    private void PlaySFX(AudioClip sfxAudio)
    {
        if (sfxSource != null && sfxAudio != null)
        {
            sfxSource.clip = sfxAudio;
            sfxSource.Play();
        }
    }
}
