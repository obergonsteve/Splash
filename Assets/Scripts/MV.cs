using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MV : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer videoMV;

    [SerializeField]
    private GameObject unlockPageGame;

    [SerializeField]
    private AudioSource gameSFX;

    [SerializeField]
    private AudioSource gameMusic;

    [SerializeField]
    private GameObject toDisable;

    void Awake()
    {
        videoMV = GetComponent<VideoPlayer>();
        gameSFX = GetComponent<AudioSource>();
        gameMusic = GetComponent<AudioSource>();
        videoMV.isLooping = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // if video is finished
        if (videoMV.frame == (long)videoMV.frameCount)
        {
            Debug.Log(videoMV.name + " Completed");
            unlockPageGame.SetActive(true);
            if (gameSFX != null)
            {
                gameSFX.Play();
            }
            if (gameMusic != null)
            {
                gameMusic.Play();
            }
            toDisable.SetActive(false);
            // reset for standby replay
            videoMV.frame = 0;
        }
    }
}
