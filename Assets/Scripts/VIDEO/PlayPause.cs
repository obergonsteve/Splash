using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayPause : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer videoPlayer;

    [SerializeField]
    private GameObject toDisable;
    [SerializeField]
    private GameObject toEnable;
    [SerializeField]
    private GameObject toEnable2;

    void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.isLooping = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if video is completed
        if (videoPlayer.frame == (long)videoPlayer.frameCount -1)
        {
            toDisable.SetActive(false);
            toEnable.SetActive(true);
            toEnable2.SetActive(true);
            Debug.Log("Learn Video Completed");
        }
    }
    public void videoPlayPause()
    {
        if (videoPlayer.isPlaying)
        {
            videoPlayer.Pause();
        }
        else
        {
            videoPlayer.Play();
            Debug.Log("Video Play");
        }
        if (videoPlayer.frame == (long)videoPlayer.frameCount -1)
        {
            videoPlayer.frame = 0;
            videoPlayer.Play();
        }
    }
}
