using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Handle screen touches
/// Mouse clicks emulate touches
/// </summary>
/// 
public class InputManager : MonoBehaviour
{
    private Vector2 touchStartPosition;
    private DateTime touchStartTime;

    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touchStartPosition = Input.GetTouch(0).position;
                touchStartTime = DateTime.Now;

                GameEvents.OnTapOn?.Invoke(touchStartPosition, 1);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchPosition = Input.GetTouch(0).position;
                GameEvents.OnTapMove?.Invoke(touchPosition, 1, touchPosition - touchStartPosition, DateTime.Now - touchStartTime); // date time = how long has it been tapped
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                var touchPosition = Input.GetTouch(0).position;
                GameEvents.OnTapOff?.Invoke(touchPosition, 1, touchPosition - touchStartPosition, DateTime.Now - touchStartTime);
            }
        }

        // emulate touch with left mouse
        else if (Input.GetMouseButtonDown(0))
        {
            GameEvents.OnTapOn?.Invoke(Input.mousePosition, 1);
        }
        else if (Input.GetMouseButton(0))
        {
            GameEvents.OnTapMove?.Invoke(Input.mousePosition, 1, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            GameEvents.OnTapOff?.Invoke(Input.mousePosition, 1, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }

        // right mouse == 2 finger swipe
        else if (Input.GetMouseButtonDown(1))
        {
            GameEvents.OnTapOn?.Invoke(Input.mousePosition, 2);
        }
        else if (Input.GetMouseButton(1))
        {
            GameEvents.OnTapMove?.Invoke(Input.mousePosition, 2, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            GameEvents.OnTapOff?.Invoke(Input.mousePosition, 2, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }
    }
}
