using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour
{
    private Animator Animation;

    [SerializeField]
    private float speed;
    private float y;

    // Start is called before the first frame update
    void Start()
    {
        Animation = GetComponent<Animator>();
        Animation.SetBool("isIdle", true);
    }

    // Update is called once per frame
    void Update()
    {
        float y = Mathf.PingPong(Time.deltaTime * speed, 1);
        transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }
}
