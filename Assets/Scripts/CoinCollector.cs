using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollector : MonoBehaviour
{
    private int totalCoins = 0;     // total coins held in 'wallet'

    public void OnEnable()
    {
        GameEvents.OnCoinsCollected += OnCoinsCollected;        // eg. on completion of a page
    }

    public void OnDisable()
    {
        GameEvents.OnCoinsCollected -= OnCoinsCollected;
    }

    private void OnCoinsCollected(int coinsCollected)
    {
        totalCoins += coinsCollected;
        GameEvents.OnTotalCoinsUpdated?.Invoke(totalCoins);     // eg. to update UI
    }
}
