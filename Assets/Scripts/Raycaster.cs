using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    [SerializeField]
    private LayerMask raycastTapLayers;                 // for 'activating' objects to drag / throw on touch 'on'

    [SerializeField]
    private LayerMask raycastDragLayers;                // for dragging object towards a point (eg. on ground, through water)

    [SerializeField]
    private LayerMask raycastThrowLayers;               // for throwing object towards a point (eg. on ground)

    private float raycastDistance = 1000f;
    //private StarfishTap capturedFlickable1 = null;    // 'flickable' object that raycast hit
    private StarfishThrow capturedFlickable = null;    // 'flickable' object that raycast hit

    [SerializeField]
    private ParticleSystem throwParticles;

    private bool throwOnRelease = true;                 // otherwise drag to release point

    private Camera mainCam;

    private void OnEnable()
    {
        GameEvents.OnTapOn += OnTapOn;
        GameEvents.OnTapMove += OnTapMove;
        GameEvents.OnTapOff += OnTapOff;
    }

    private void OnDisable()
    {
        GameEvents.OnTapOn -= OnTapOn;
        GameEvents.OnTapMove -= OnTapMove;
        GameEvents.OnTapOff -= OnTapOff;
    }

    private void Start()
    {
        // get Camera.main once only for efficiency
        mainCam = Camera.main;
    }

    // event handlers for touch on/ move / off events

    private void OnTapOn(Vector2 screenPosition, int touchCount)
    {
        TapRaycast(screenPosition);
    }

    private void OnTapMove(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart)
    {
        DragToRaycast(screenPosition);
    }

    private void OnTapOff(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart)
    {
        ThrowToRaycast(screenPosition);
    }

    // raycast for target collider layer from touch screenPosition and Flick component
    // activate the hit Flick (eg. animation)
    private void TapRaycast(Vector2 screenPosition)
    {
        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastTapLayers);

        if (hitInfo.transform != null)
        {
            Debug.Log($"TapRaycast HIT: {hitInfo.transform.name}");
            //capturedFlickable1 = hitInfo.transform.GetComponent<StarfishTap>();
            capturedFlickable = hitInfo.transform.GetComponent<StarfishThrow>();

            if (capturedFlickable != null)
            {
                capturedFlickable.Tap();
            }
            //else if (capturedFlickable1 != null)
            //{
            //    capturedFlickable1.Tap();
            //}
        }
    }

    // raycast for throw 'destination' layer from touch screenPosition, drag hitFlick to hitPoint
    private void DragToRaycast(Vector2 screenPosition)
    {
        if (capturedFlickable == null)
            return;

        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastDragLayers);

        if (hitInfo.transform != null)
        {
            Debug.Log($"DragToRaycast HIT: {hitInfo.transform.name}");
            capturedFlickable.Drag(hitInfo.point, false);       // hitFlick maintains its original y pos
        }
    }

    // raycast for throw 'destination' layer from touch screenPosition, throw hitFlick in direction of hitPoint
    // simply drag if still raycasting capturedFlickable
    private void ThrowToRaycast(Vector2 screenPosition)
    {
        //if (capturedFlickable1 == null)
        //    return;

        if (capturedFlickable == null)
            return;

        // raycast to detect what was touched
        RaycastHit hitInfo = DoRaycast(screenPosition, raycastThrowLayers);

        // drag if raycasting hitFlick, else throw in direction of hit point
        if (hitInfo.transform != null)
        {
            Debug.Log($"ThrowToRaycast HIT: {hitInfo.transform.name}");

            if (throwOnRelease)
            {
                if (hitInfo.transform == capturedFlickable.transform)   // still raycasting same flickable object
                {
                    capturedFlickable.Drag(hitInfo.point, true);
                    capturedFlickable.Release();           // drop down/shrink if in target zone, else return to start position
                }
                else
                {
                    if (throwParticles != null)
                    {
                        throwParticles.transform.position = hitInfo.point;
                        throwParticles.Play();
                    }

                    capturedFlickable.Throw(hitInfo.point);
                }
            }
            else
            {
                capturedFlickable.Drag(hitInfo.point, false);
                capturedFlickable.Release();           // drop down/shrink if in target zone, else return to start position
            }

            capturedFlickable = null;
        }
        else    // raycast didn't hit anything in raycastThrowLayers, so just drag or snap back
        {
            Debug.Log($"ThrowToRaycast NO HIT!");
            //var touchOffWorldPos = mainCam.ScreenToWorldPoint(screenPosition);

            //capturedFlickable.Throw(touchOffWorldPos);
            capturedFlickable.Release();           // drop down/shrink if in target zone, else return to start position
            capturedFlickable = null;
        }
    }

    private RaycastHit DoRaycast(Vector2 touchPosition, LayerMask layers)
    {
        Ray ray = mainCam.ScreenPointToRay(touchPosition);

        // raycast to detect what was touched
        RaycastHit hitInfo;
        Physics.Raycast(ray, out hitInfo, raycastDistance, layers);

        return hitInfo;
    }
}