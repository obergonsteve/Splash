using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevate : MonoBehaviour
{
    public float mvDuration;
    public float elevateDistance;

    // Start is called before the first frame update
    void Start()
    {
        // Detect this game object
        // Change game object (y) position to 'elevateDistance' when game starts
    }

    // Update is called once per frame
    void Update()
    {
        // Move game object position to original position after 'mvDuration'
        // Use lerp
    }
}
