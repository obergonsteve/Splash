using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowSuccess : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var rb = other.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.useGravity = true;
            rb.isKinematic = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
}
