using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARTrackedImageManager))]
[RequireComponent(typeof(ARAnchorManager))]
[RequireComponent(typeof(ARRaycastManager))]
public class ImageRecognition : MonoBehaviour
{
    [SerializeField] 
    private GameObject[] bookPages;
    private Dictionary<string, GameObject> spawnedPrefabs = new Dictionary<string, GameObject>();
    private ARTrackedImageManager m_trackedImageManager;
    private ARAnchorManager m_AnchorManager;
    private ARRaycastManager m_RaycastManager;

    private void Awake()
    {
        m_trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        foreach (GameObject bookPage in bookPages)
        {
            GameObject newBookPage = Instantiate(bookPage, Vector3.zero, Quaternion.identity);
            newBookPage.name = bookPage.name;
            spawnedPrefabs.Add(bookPage.name, newBookPage);
        }
    }

    private void OnEnable() => m_trackedImageManager.trackedImagesChanged += OnChanged;
    //in unity => is just a shortcut for {} 

    private void OnDisable() => m_trackedImageManager.trackedImagesChanged -= OnChanged;

    private void OnChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            spawnedPrefabs[trackedImage.name].SetActive(false);
        }
    }

    private void UpdateImage(ARTrackedImage trackedImage)
    {
        string name = trackedImage.referenceImage.name;
        Vector3 position = trackedImage.transform.position;

        GameObject bookPage = spawnedPrefabs[name];
        bookPage.transform.position = position;
        bookPage.SetActive(true);
        Debug.Log("Image Target Recognized: " + bookPage.name);

        foreach (GameObject go in spawnedPrefabs.Values) //what is this?
        {
            if (go.name != name)
            {
                go.SetActive(false);
            }
        }
    }

}