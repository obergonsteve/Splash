using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to enable an object to be dragged and thrown
/// in 3D space via touch / mouse.
/// Object follows raycast from touch screen position.
/// </summary>

[RequireComponent(typeof(Rigidbody))]
public class StarfishThrow : MonoBehaviour
{
    private bool tapped = false;            // ie. 'grabbed' by raycast
    //private bool throwing = false;        // ie. 'let go'

    //[SerializeField]
    private float draggingSpeed = 20f;      
    private float maxDragDistance = 7f;     // automatically thrown if exceeded
    private float maxVelocity = 40f;        // clamped

    [SerializeField]
    private bool usePhysicsForThrow = false;
    private float throwTime = 0.5f;         // time to reach destination if not physics
    private float throwLiftY = 0.5f;        // lift speed if not physics
    private float maxThrowLiftY = 12f;      // to limit height

    [SerializeField]
    private float throwSpeedY = 1f;         // vertical lift for throw (if physics)
    [SerializeField]
    private float throwForceFactor = 5f;    // throw force factor (if physics)

    private Vector3 tapPosition;
    private bool hittingThrowTarget = false;// is object hitting target trigger?  (eg. water)

    private float disappearTime = 0.5f;     // scale to zero
    private float snapTime = 0.1f;          // move back to start

    private int throwTweenX;
    private int throwTweenY;
    private int throwTweenZ;

    [SerializeField]
    private int coinCollectedOnTap = 10;
    [SerializeField]
    private int coinCollectedOnThrow = 20;

    private Rigidbody rb;

    [SerializeField]
    private AudioSource starfishTapVO;
    [SerializeField]
    private AudioSource starfishThrowVO;

    [SerializeField]
    private Animator Animation;
    [SerializeField]
    private GameObject colliderName;

    public bool tapDone;
    public bool throwDone;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = !usePhysicsForThrow;

        starfishThrowVO = GetComponent<AudioSource>();
        Animation = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        // limit velocity
        if (usePhysicsForThrow)
        {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocity);
        }
    }

    public void Tap()
    {
        if (tapped)
        {
            Debug.Log($"{colliderName.name} already tapped!!");
            return;
        }

        tapped = true;
        tapPosition = transform.position;

        // TODO: trigger animation?  play audio?
        starfishTapVO.Play(0);
        Animation.SetBool("isTap", true);
        // fire activated event
        tapDone = true;
        Debug.Log("Tap Done: " + colliderName.name);
        GameEvents.OnFlickableTapped?.Invoke(this);
        GameEvents.OnCoinsCollected?.Invoke(coinCollectedOnTap);
        Debug.Log("Coins Collected: " + coinCollectedOnTap);
    }

    // on touch move
    // released == true if touch/mouse released
    public void Drag(Vector3 toPosition, bool released)
    {
        if (!tapped)
            return;

        // fire dragged event
        GameEvents.OnFlickableDragged?.Invoke(this);

        //if ((toPosition - tapPosition).magnitude > maxDragDistance)
        //{
        //    Throw(toPosition);
        //}
        //else
        {
            // maintain original y position
            // move a step closer to toPosition
            float step = draggingSpeed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(toPosition.x, tapPosition.y, toPosition.z), step);
            //transform.position = Vector3.MoveTowards(transform.position, toPosition, step);
            starfishThrowVO.Play(0);
            Animation.SetBool("isThrow", true);
            Debug.Log("Throw Animation Activated: " + colliderName.name);
            if (released)            // fire thrown event if touch released
                GameEvents.OnFlickableThrown?.Invoke(this);
        }

        // TODO: trigger animation?  play audio?

        //// fire dragged event
        //GameEvents.OnFlickableDragged?.Invoke(this);
    }

    // on touch off
    public void Throw(Vector3 hitPosition)
    {
        if (!tapped)
            return;

        Debug.Log("Throw: " + colliderName.name);

        // throw to starting y position
        //Vector3 throwTarget = new Vector3(hitPosition.x - (transform.localScale.x / 2f), activationPosition.y, hitPosition.z - (transform.localScale.z / 2f));
        Vector3 throwTarget = new Vector3(hitPosition.x, tapPosition.y, hitPosition.z);

        var throwDirection = throwTarget - transform.position;        // represents direction and speed

        //var throwDistance = Vector3.Distance(transform.position, throwTarget);

        // fire thrown event - for PageCompletion
        GameEvents.OnFlickableThrown?.Invoke(this);

        if (usePhysicsForThrow)
        {
            // lift force in throwDirection
            var verticalLift = throwSpeedY * throwDirection.magnitude;

            // greater force applied with greater distance between transform.position and toPosition
            //rb.AddForce(new Vector3(throwDirection.x, verticalLift, throwDirection.z) * throwForceFactor, ForceMode.Impulse);
            rb.AddForce(throwDirection * throwForceFactor, ForceMode.Impulse);
            tapped = false;
        }
        else            // tween x, y (lift) and z axes separately to simulate arc / gravity
        {
            var throwY = throwLiftY * throwDirection.magnitude;     // higher lift for greater distance
            throwY = Mathf.Clamp(throwY, 0f, maxThrowLiftY);

            // lift (y)
            throwTweenY = LeanTween.moveY(gameObject, throwY, throwTime / 2f)
                        .setEaseInOutQuad()
                        .setOnComplete(() =>
                        {
                            // back to start y
                            DropToStartY();
                        }).id;

            // move z
            throwTweenZ = LeanTween.moveZ(gameObject, throwTarget.z, throwTime)
                        .setEaseLinear().id;
            //.setEaseInSine().id;
            //.setEaseOutSine().id;

            // move x
            throwTweenX = LeanTween.moveX(gameObject, throwTarget.x, throwTime)
                        .setEaseLinear()
                        //.setEaseOutSine()
                        //.setEaseInSine()
                        .setOnComplete(() =>
                        {
                            tapped = false;
                        }).id;
            starfishThrowVO.Play(0);
            Animation.SetBool("isThrow", true);
            Debug.Log("Throw Animation Activated: " + colliderName.name);
        }

        // TODO: trigger animation?  play audio?
      
        //// fire thrown event
        //GameEvents.OnFlickableThrown?.Invoke(this);
    }

    private void StopThrow()
    {
        LeanTween.cancel(throwTweenX);
        LeanTween.cancel(throwTweenY);
        LeanTween.cancel(throwTweenZ);

        tapped = false;
    }

    private void DropToStartY()
    {
        // back to start y
        throwTweenY = LeanTween.moveY(gameObject, tapPosition.y, throwTime / 2f)
                    .setEaseInQuad().id;
    }

    private void Disappear()
    {
        LeanTween.scale(gameObject, Vector3.zero, disappearTime)
                    .setEaseOutQuad();
    }

    private void SnapBackToStart()
    {
        LeanTween.move(gameObject, tapPosition, snapTime)
                        .setEaseOutQuad();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!usePhysicsForThrow)
        {
            //var collisionPoint = collision.contacts[0].point;
            //Debug.Log($"{name} hit {collision.gameObject.name}");
            StopThrow();        // cancel tweens
            DropToStartY();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ThrowTarget"))
        {
            hittingThrowTarget = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("ThrowTarget"))
        {
            hittingThrowTarget = false;
        }
    }

    public void Release()
    {
        if (hittingThrowTarget)
        {
            Disappear();
            // Add water splash
            GameEvents.OnCoinsCollected?.Invoke(coinCollectedOnThrow);
            Debug.Log("Coins Collected: " + coinCollectedOnThrow);
        }
        else
        {
            SnapBackToStart();
        }
    }
}