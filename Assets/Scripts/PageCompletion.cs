using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageCompletion : MonoBehaviour
{
    public PageGame pageGame;
    private int completionCount;

    //[SerializeField]
    //private List<StarfishTap> starfishToTap1;

    [SerializeField]
    private List<StarfishThrow> starfishToTap;

    [SerializeField]
    private List<StarfishThrow> starfishToThrow;

    [SerializeField]
    private GameObject toDisable;
    [SerializeField]
    private GameObject toEnable;
    //[SerializeField]
    //private GameObject toEnable2;

    // Start is called before the first frame update
    void Start()
    {
        pageGame = GetComponent<PageGame>();
    }

   
    private void OnEnable() // tap, throw, spawn (page 2-6)
    {
        GameEvents.OnFlickableTapped += OnFlickableTapped;
        GameEvents.OnFlickableThrown += OnFlickableThrown;
    }

    private void OnDisable()
    {
        GameEvents.OnFlickableTapped -= OnFlickableTapped;
        GameEvents.OnFlickableThrown -= OnFlickableThrown;
    }


    private void OnFlickableTapped(StarfishThrow tapped)
    {
        CheckIfComplete(tapped);
    }

    private void OnFlickableThrown(StarfishThrow thrown)
    {
        CheckIfComplete(thrown);
    }


    private void CheckIfComplete(StarfishThrow tapped)
    {
        var starfishTapped = tapped.GetComponent<StarfishThrow>();
        var starfishThrown = tapped.GetComponent<StarfishThrow>();

        if (starfishTapped != null && starfishThrown != null)
        {
            if (IsAllTapped() && IsAllThrown() == true) // && IsAllSpawned
            {
                PageComplete();
                Debug.Log($"CheckIfComplete: page {pageGame.name} complete!");
            }
            else
                Debug.Log($"CheckIfComplete: page {pageGame.name} NOT complete!");
        }
    }

    private bool IsAllTapped()
    {
        foreach(var starfish in starfishToTap)
        {
            if (!starfish.tapDone)
            {
                return false;
            }
        }
        Debug.Log("All Is Tapped: " + pageGame.name);
        return true;
    }

    private bool IsAllThrown()
    {
        foreach (var starfish in starfishToThrow)
        {
            if (!starfish.throwDone)
            {
                return false;
            }
        }
        Debug.Log("All Is Thrown: " + pageGame.name);
        return true;
    }

    // stardrop completion (title page)

    // video completion MV (page 1)

    // splash world 5 completion (page 5)

    // splash world 6 completion (page 6)

    private void PageComplete()
    {
        //completionCount++;
        pageGame.SetStatus(PageGame.PageStatus.Completed);
        Debug.Log("Page Completed: " + pageGame.name);
        //GameEvents.OnPageGameCompleted?.Invoke(pageGame, completionCount);
        toDisable.SetActive(false);
        toEnable.SetActive(true);
        //toEnable2.SetActive(true);
    }
}
