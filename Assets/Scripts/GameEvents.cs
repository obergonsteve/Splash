using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents
{
    // GameManager
    public delegate void OnARCameraPoseChangedDelegate(Pose cameraPose, Pose ARCameraPoseDelta);
    public static OnARCameraPoseChangedDelegate OnARCameraPoseChanged;

    // PageGame
    public delegate void OnPageGameStatusChangedDelegate(PageGame.PageStatus prevStatus, PageGame page);
    public static OnPageGameStatusChangedDelegate OnPageGameStatusChanged;

    public delegate void OnPageGameCompletedDelegate(PageGame page, int completionCount);
    public static OnPageGameCompletedDelegate OnPageGameCompleted;

    public delegate void OnPageGameBlockedDelegate(PageGame page, string reason);
    public static OnPageGameBlockedDelegate OnPageGameBlocked;

    public delegate void OnPageGameLookAtRaycastDelegate(Transform lookingAt);
    public static OnPageGameLookAtRaycastDelegate OnPageGameLookAtRaycast;

    //public delegate void OnPageGameTouchRaycastDelegate(Transform touched, Vector3 hitPoint);
    //public static OnPageGameTouchRaycastDelegate OnPageGameTapRaycast;

    // SplashWorld
    public delegate void OnSplashWorldEnterDelegate(SplashWorld splashWorld, PageGame page);
    public static OnSplashWorldEnterDelegate OnSplashWorldEnter;

    public delegate void OnSplashWorldExitDelegate(SplashWorld splashWorld, PageGame page);
    public static OnSplashWorldExitDelegate OnSplashWorldExit;

    public delegate void OnSplashWorldLookAtRaycastDelegate(Transform lookingAt);
    public static OnSplashWorldLookAtRaycastDelegate OnSplashWorldLookAtRaycast;

    public delegate void OnSplashWorldTouchRaycastDelegate(Transform touched, bool isExit);
    public static OnSplashWorldTouchRaycastDelegate OnSplashWorldTapRaycast;

    // Tap Input
    public delegate void OnTapOnDelegate(Vector2 screenPosition, int touchCount);
    public static OnTapOnDelegate OnTapOn;

    public delegate void OnTapMoveDelegate(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart);
    public static OnTapMoveDelegate OnTapMove;

    public delegate void OnTapOffDelegate(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart);
    public static OnTapOffDelegate OnTapOff;

    // Drag / Throw
    //public delegate void OnFlickableActivated1Delegate(StarfishTap flickObject);
    //public static OnFlickableActivated1Delegate OnFlickableTapped1;

    public delegate void OnFlickableActivatedDelegate(StarfishThrow flickObject);
    public static OnFlickableActivatedDelegate OnFlickableTapped;

    public delegate void OnFlickableDraggedDelegate(StarfishThrow flickObject);
    public static OnFlickableDraggedDelegate OnFlickableDragged;

    public delegate void OnFlickableThrownDelegate(StarfishThrow flickObject);
    public static OnFlickableThrownDelegate OnFlickableThrown;

    // UI
    public delegate void OnFadeToBlackDelegate(Action onFade, string message);
    public static OnFadeToBlackDelegate OnFadeToBlack;

    // Coins
    public delegate void OnCoinsCollectedDelegate(int coinsCollected);
    public static OnCoinsCollectedDelegate OnCoinsCollected;                // fired on successful completion of a page

    public delegate void OnTotalCoinsUpdatedDelegate(int totalCoins);       // total coins held in 'wallet'
    public static OnTotalCoinsUpdatedDelegate OnTotalCoinsUpdated;          // fired on update of total coins held (eg. to update UI)

}

