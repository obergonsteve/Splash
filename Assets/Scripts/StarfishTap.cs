//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class StarfishTap : MonoBehaviour
//{
//    private bool tapped = false;            // ie. 'grabbed' by raycast
//    private Vector3 tapPosition;

//    [SerializeField]
//    private AudioSource starfishTapVO;

//    [SerializeField]
//    private Animator Animation;

//    public GameObject colliderName;

//    [SerializeField]
//    private int coinCollectedOnTap = 10;
    
//    public bool tapDone;

//    // Start is called before the first frame update
//    void Start()
//    {
//        starfishTapVO = GetComponent<AudioSource>();
//        Animation = GetComponent<Animator>();
//    }

//    private void Update()
//    {

//    }

//    public void Tap()
//    {
//        if (tapped)
//            return;
//        tapped = true;
//        tapPosition = transform.position;

//        // TODO: trigger animation?  play audio?
//        starfishTapVO.Play(0);

//        if(Animation != null)
//        {
//            Animation.SetBool("isTap", true);
//        }
//        // fire activated event
//        tapDone = true;
//        Debug.Log("Tap Done: " + colliderName.name);
//        GameEvents.OnFlickableTapped1?.Invoke(this);
//        GameEvents.OnCoinsCollected?.Invoke(coinCollectedOnTap);
//        Debug.Log("Coins Collected: " + coinCollectedOnTap);
//    }

//    public void Release()
//    {
//        if (!tapped)
//            return;
//        tapped = false;

//        if (Animation != null)
//        {
//            Animation.SetBool("isIdle", true);
//        }
//    }
//}

